**TOTP_script**

---

Bash script to generate TOTP and maintain secrets encrypted with RSA

_Requires:_
* openssl
* oath-toolkit

_Useage:_

* Generate TOTP code for any one of the saved account by [name]

    get_oath_code.sh [name]

* Save a new account [name] by supplying [secret] secret key without any spaces

    get_oath_code.sh save [name] [secret]

* Delete saved account by [name]

    get_oath_code.sh del [name]

* List all the saved accounts
    
    get_oath_code.sh list

* Generate TOTP for all the saved accounts

    get_oath_code.sh all
