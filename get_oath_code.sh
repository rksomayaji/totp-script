SECRET_FILE="$HOME/.config/oath-secrets"
PRIVATE_KEY_FILE="$HOME/.ssh/private.pem"
PUBLIC_KEY_FILE="$HOME/public.pem"

function print_help() {
    echo "Useage:"
    echo "get_oath_code.sh [name]"
    echo "********************************************************************************************"
    echo ""
    echo "get_oath_code.sh save [name] [secret key]         : save account with [name] and [secret key]"
    echo ""
    echo "get_oath_code.sh del [name]                       : delete account with [name]"
    echo ""
    echo "get_oath_code.sh list                             : list all the saved accounts"
    echo ""
    echo "get_oath_code.sh all                              : show all totp codes with account name"
    echo ""
    echo "********************************************************************************************"
}

if ! [ -f "$SECRET_FILE" ] ; then 
    echo "$SECRET_FILE does not exists. No secrets saved."
    echo ""
    print_help
    if [ $# -le 1 ]; then
        exit 0
    fi
elif ! [ -f "$PUBLIC_KEY_FILE" ] || ! [ -f "$PRIVATE_KEY_FILE" ] ; then
    echo "Generating new secrets"
    if ! [ -d "$HOME/.ssh" ] ; then
        mkdir "$HOME/.ssh"
    fi
    openssl genrsa -out "$PRIVATE_KEY_FILE" 4096
    openssl rsa -in "$PRIVATE_KEY_FILE" -out "$PUBLIC_KEY_FILE" -outform PEM -pubout
else
    openssl rsautl -decrypt -inkey "$PRIVATE_KEY_FILE" -in $SECRET_FILE -out decrypted.txt
fi

if [ $# -eq 0 ] ; then
    print_help
elif [ "$1" = "save" ] ; then
	echo "$2:$3" >> decrypted.txt
elif [ "$1" = "del" ] ; then
    while read SECRET; do
        if [ $(echo "$SECRET" | cut -d ":" -f 1) != $2 ]; then
            echo "$SECRET" >> temp
	fi
    done < decrypted.txt
    mv temp decrypted.txt
elif [ "$1" = "list" ] ; then
    while read SECRET; do
	echo $(echo "$SECRET" | cut -d ":" -f 1)
    done < decrypted.txt
elif [ "$1" = "all" ]; then
    while read SECRET; do
        echo $(echo "$SECRET" | cut -d ":" -f 1) - $(oathtool --totp -b "$(echo "$SECRET" | cut -d":" -f 2)")
    done < decrypted.txt
elif [ "$1" = "help" ]; then
    print_help
else
    while read SECRETS; do
	if [ $(echo "$SECRETS" | cut -d ":" -f 1 ) = $1 ]; then
	    oathtool --totp -b "$(echo "$SECRETS"| cut -d ':' -f 2)"
	else
	    echo "No secret found for $1"
	fi
    done < decrypted.txt
fi

openssl rsautl -encrypt -inkey "$PUBLIC_KEY_FILE" -pubin -in decrypted.txt -out $SECRET_FILE

rm decrypted.txt
